from django.urls import include, path
from django.contrib.auth import views as auviews

from . import views


urlpatterns = [
    path('registration/', views.register, name='register'),
    path('login/', auviews.LoginView.as_view(template_name='mauth/login.html'), name='login'),
    path('logout/', auviews.LogoutView.as_view(template_name='mauth/login.html'), name='logout'),
]