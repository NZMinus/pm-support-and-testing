Дисциплина: Поддержка и тестирование ПМ
=====================
Тема: Интерактивный редактор исторических битв с использованием геоданных
-----------------------------------
**Участники:**

1. Backend разработчик -> Орехов С.

2. Frontend разработчик и CI/CD -> Зайцев Н.

3. Тестирование -> Петров А.

4. Документация -> Самойлова А.

5. Дизайн -> Фёдорова М.

***Ссылка на демо сайт:*** [Heroku app](https://editor-of-historical-battles.herokuapp.com/)