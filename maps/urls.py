from django.urls import path
from . import views

urlpatterns = [
    path('', views.render_main, name='main'),
    path('battles/', views.show_battles, name='battles'),
    path('battle/<bid>/', views.show_battle, name='battle'),
    path('add_battle/', views.add_battle, name='add_battle'),
    path('wars/', views.show_wars, name='wars'),
    path('add_war/', views.add_war, name='add_war'),
    path('upd_loc/<bid>/', views.update_location, name='upd_loc'),
]