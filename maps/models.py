from django.db import models


class War(models.Model):
    war_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    about = models.CharField(max_length=400)
    startDate = models.DateField()
    finishDate = models.DateField()


class Battle(models.Model):
    battle_id = models.AutoField(primary_key=True)
    coordx = models.FloatField(null=True)
    coordy = models.FloatField(null=True)
    name = models.CharField(max_length=100)
    startDate = models.DateField()
    finishDate = models.DateField()
