from django.contrib import admin

from .models import Battle
from .models import War

admin.site.register(Battle)
admin.site.register(War)
