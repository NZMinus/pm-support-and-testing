from django.shortcuts import render, redirect
from .models import Battle
from .models import War
from .forms import AddBattleForm
from .forms import AddWarForm
from .forms import EditBattleForm
from .forms import UpdateLocationForm


def render_main(request):
    return render(request, 'maps/index.html')


def show_wars(request):
    add_war_form = AddWarForm(request.POST)
    wars = War.objects.all()
    context = {
        'wars': wars,
        'add_war_form': add_war_form,
    }
    return render(request, 'maps/add_war.html', context)


def add_war(request):
    if request.POST:
        add_war_form = AddWarForm(request.POST)
        if add_war_form.is_valid():
            add_war_form.save()
    return redirect('wars')


def show_battles(request):
    add_battle_form = AddBattleForm(request.POST)
    battles = Battle.objects.all()
    print(battles)
    context = {'add_battle_form': add_battle_form,
               'battles': battles}
    return render(request, 'maps/battles.html', context)


def show_battle(request, bid):
    edit_battle_form = EditBattleForm(request.POST)
    update_location_form = UpdateLocationForm(request.POST)
    battle_info = Battle.objects.filter(battle_id=bid)[0]
    context = {
        'edit_battle_form': edit_battle_form,
        'update_location_form': update_location_form,
        'battle_info': battle_info,
    }
    return render(request, 'maps/content.html', context)


def update_location(request, bid):
    print('i\'m here')
    if request.method == 'POST':
        update_location_form = UpdateLocationForm(request.POST)
        if update_location_form.is_valid():
            update_location_form.save(commit=False)
            cx = update_location_form.cleaned_data['cx']
            cy = update_location_form.cleaned_data['cy']
            battle = Battle.objects.filter(battle_id=bid)[0]
            battle.coordx = cx
            battle.coordy = cy
            battle.save()
    return redirect('battle', bid)


def add_battle(request):
    if request.method == 'POST':
        add_battle_form = AddBattleForm(request.POST)
        if add_battle_form.is_valid():
            add_battle_form.save()
    return redirect('battles')


def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()
            return render(request, 'mauth/login.html')
    else:
        user_form = UserRegistrationForm()
    return render(request, 'mauth/registration.html', {'user_form': user_form})
