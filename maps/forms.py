from django import forms
from django.contrib.auth.models import User
from django.forms.fields import DateField
from .models import Battle, War


class AddWarForm(forms.ModelForm):
    dates = [str(i) for i in range(1, 2021)]
    startDate = forms.DateField(label='Дата начала войны', widget=forms.SelectDateWidget(years=dates))
    finishDate = forms.DateField(label='Дата конца войны', widget=forms.SelectDateWidget(years=dates))

    class Meta:
        model = War
        fields = ('name', 'about', 'startDate', 'finishDate')


class AddBattleForm(forms.ModelForm):
    dates = [str(i) for i in range(1, 2021)]
    startDate = forms.DateField(label='Дата начала битвы', widget=forms.SelectDateWidget(years=dates))
    finishDate = forms.DateField(label='Дата конца битвы', widget=forms.SelectDateWidget(years=dates))

    class Meta:
        model = Battle
        fields = ('name', 'startDate', 'finishDate')


class EditBattleForm(forms.ModelForm):
    dates = [str(i) for i in range(1, 2021)]
    startDate = forms.DateField(label='Дата начала битвы', widget=forms.SelectDateWidget(years=dates))
    finishDate = forms.DateField(label='Дата конца битвы', widget=forms.SelectDateWidget(years=dates))

    class Meta:
        model = Battle
        fields = ('startDate', 'finishDate')


class UpdateLocationForm(forms.ModelForm):
    cx = forms.CharField(widget=forms.HiddenInput())
    cy = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = Battle
        fields = ()
